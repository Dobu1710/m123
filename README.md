# Portfolio Module 123 Server in Betriebnahme

![bild](/images/teaser-banner-primergy-960x540px_tcm22-6552944_tcm22-6286607-32.jpg)





## Inhaltsverzeichnis
1. [Dokumenation DHCP Server in Filius Konfigurieren](/DHCP/)

   1.1 [Dokumentation DHCP auf Ubuntu Server 2022](/DHCP/DHCP%20Konfiguration%20Ubuntu%20server/ReadME.md)
   
2. [Dokumentation DNS Konfiguration Windows Server](/DNS/README.md)
3. [Dokumentation Fileshare Konfiguration Ubuntu Desktop](/Fileshare/README.md)


