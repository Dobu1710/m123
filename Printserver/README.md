# Konfiguration Windows Printserver
## VM erstellen:
Erstelle die VM. Wen du sie erstellt hast stelle den Netzwerkadapter auf Costum_M117Lab. 
Dann erstelle einen neuen Netzwerkadapter der auf Bridge eingestellt ist und füge ihne dann hinzu.
Dies sollte dann so aussehen:

![Beispielbild](/images/Bridge%20und%20costumnetzwerkadapter.jpg)


## Windows Server installation
Wen du die Vm erstellt hast und die Netzwerkadpter richtig eingestellt hast installier auf deiner VM Windows Server.



## Netzwerkadapter einstellen:
Stelle den Costum_M117Lab so ein:

![Beispielbild](/images/costum%20m117lab%20adapter.jpg)



Stelle den Bridge Adapter wie folgt ein:


![Beispielbild](/images/bridge%20netzwerkadapter.jpg)


## Printserver Konfiguration
### Treiber installation
Öffne den Server Manager und klicke auf Tools und wähle Print Managment an:

![Beispielbild](/images/Print%20managmetn.jpg)




Wen sich das Print Managment geöffnet hat rechtsklicke auf driver und wähle add drivers.
Dann kommt die Startseite klicke auf Next beim nächsten Schritt klicke dies an:

![Beispielbild](/images/Prozessor.jpg)



Beim nächsten Feld naviegiere anhand deines Druckers und suche den passenden Treiber:

![Beispielbild](/images/drucker%20und%20treiber.jpg)




Zum Schluss schliesse die Treiber installation ab:

![Beispielbild](/images/Finish%20treiber%20installation.jpg)

### Drucker hinzufügen:

Rechtsklicke im Print Managment auf Printers und wähle  add Printer.
Beim Nächsten Schritt wähle search for network printer:

![Beispielbild](/images/search%20for%20network%20printer.jpg)


Beim nächsten Schritte wähle den Passenden Drucker aus:

![Beispielbild](/images/drucker%20auswahl.jpg)



Beim nächsten Fenster überprüfe ob das Kästen mit Sharing aktiviert ist wen ja klicke weiter.
Beim nächsten Fenster klicke weiter. Dann warte bis die Drucker installation abgeschlossen ist.

![Beispielbild](/images/finish%20drucker%20hinzufügen.jpg)


### Überprüfung:
Rechsklicke auf den Drucker und wähle Print test Page. Wen es etwas ausdruckt dann hast du den Treiber und den Drucker erfolgreich hinzugefügt.



## Konfiguration Windows Client:
Stelle den Netzwerkadapter auf Costum_M117Lab ein.
Stelle dein Netwerkadapter so ein:

![Beispielbild](/images/Client%20adapter%20konfig.jpg)



### Drucker auf Client hinzufügen:
Öffne dein Explorer und gehe auf Netzwerk.
Gib dort \\ und die IP deines Servers ein dies sollte dann ungefair so aussehen:

![Beispielbild](/images/mit%20ip%20drucker%20verbinden.jpg)




Dann kommt ein Fenster wo du Login Daten vom Server eingeben musst:

![Beispielbild](/images/benutzerabfrage%20print.jpg)



Dann solltest du eigentlich dich mit dem Drucker verbinden können und über den Printserver ein Blatt ausdrucken können.
Es kommt immmer die Meldung i cannot connect to the Printer:

![Beispielbild](/images/cannot%20connect.jpg)



Was genau das Problem ist konnten wir nicht wirklich herausfinden


## Troubelshooting
- Ich habe in der Registery etwas angepasst
- Ich habe Print spoler neu gestartet.
- Treiberdatei in die richtige Ordner kopiert.
- Drucker entfernt und wieder hinzugefügt.




















