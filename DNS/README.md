# DNS Konfiguration Windows Server 2022
## VM erstellen
1. Bei der erstellung der VM gibt es ein kleine Abweichung als zu denen anderen VM wo wir bis jetz erstellt haben.
Es kommt ein Fenster wo man den Product Key eingeben muss und ebenfalls muss du ein Konto mit Passwort generieren.
Dies sieht dann so aus:

![Lokales Bild](/images/Bild1.jpg)




2.Bevor sie mit installation des Windows Server beginnen können müssen sie noch ihren Netzwerkadapter auf Costum (M117_Lab) einstellen. Füge ebenfals einen 2 Adapter hinzu und stelle ihn auf NAT ein. Die sollt dann so aussehen:

![Lokales Bild](/images/adapter%20teil%202.jpg)






## Windows Installation
1. Starten Sie die VM und warten Sie bis das Setup startet.
2. Danach diese Version auswählen:

![Lokales Bild](/images/Windows%20Server%20version.jpg)



3. Warten Sie bis Windows fertig installiert ist und rebooten sie dann.
4. Installieren Sie die VM Tools.

## IP Vergabe
1. Sie müssen ihrem Server jetzt eine statiische IP vergeben. Den rest sollten sie wie folgt einstellen:

![Lokales Bild](/images/Statische%20Ip%20vergabe.jpg)





Der alternate DNS Server trägt die Adresse 8.8.8.8 weil dies der Google DNS ist.
Der preferred DNS server trägt die Adresse 127.0.0.1 für loopback. 
Loopack wird als preferred dns server genommen weil wenn du das Loopback-Interface als bevorzugten DNS-Server einstellst, kann der Computer Anfragen an sich selbst richten, ohne das externe Netzwerk zu nutzen. Das ist hilfreich, wenn der Computer lokale DNS-Anfragen für eigene Dienste oder Tests bearbeiten soll.



## DNS Server installieren
1. Starten Sie den Server Manager
2. Navigiere zum Dashboard und wähle Rollen und Funktionien hinzufügen:

![Lokales Bild](/images/rolles%20and%20feature%20hinzufügen.jpg)




3. Danach öffnet sich ein neues Fenster klicken sie auf weiter und wählen sie bei der nexten Fenster rollenbasierte installation aus :

![Lokales Bild](/images/rollenbasiert%20feature%20installation.jpg)




4. Beim nächsten Fenster lassen sie alles wie es ist und wählen weiter.
5. Beim nächsten Schritt müssen sie die server rolle auswählen. In unserem Fall wäre dies DNS Server:

![Lokales Bild](/images/Serverrolle%20auswählen.%20jpg.png)

6. Danach öffnet sich ein Fenster klicke hier auf add features:

![Lokales Bild](/images/Bild%20add%20features.jpg)




7. Nach diesem Schritt klicke auf weiter bis du zur installation kommst. Danach installierst du den DNS und wartest bis die Installation abgeschlossen ist.

## DNS Konfiguration
### Zonen erstellen:
Es gibt mehrere  Zonen bei einem DNS Server. Ich werde aber nur zwei einrichten die Forward und die Reverse-lookup Zone. Bei der Forward-Lookup ist es so das der DNS-Server eine herkömliche Webseit- Domäne in eine IP-Adresse umwandeln. Bei der Reserve-Lookup ist dies genau das umgekehrte.
Beim Server Manager unter Tools können sie auf DNS klicken und dann werden sie weitergeleitet auf die DNS-Applikation dort können sie den DNS und dessen Zonen verwalten.

1. Forward-Lookup Zone erstellen:

Rechtsklicke auf Forward-Lookup Zone und wähle New Zone:


![Lokales Bild](/images/Forward%20Zone%20erstelen.jpg)





1.1 Danach erscheint die Startseite von Zonen erstellen. Klicke auf weiter und wähle im nächsten Fenster Primary Zone aus:

![Lokales Bild](/images/Primary%20zone.jpg)







1.2 Erstelle ein Zone name :

![Lokales Bild](/images/Zone%20name.jpg)








1.3 Beim nächsten Schritt klicke auf weiter und wähle beim nächsten Schritt do not allow dynamics updates:

![Lokales Bild](/images/dynamics%20updates.jpg)








1.4 Schliesse beim nächsten Schritt die forward-lookup Zone erstellung ab.

2. Rerverse-Lookup Zone erstellen:
Mache alle Schritt wie bei der Forward-Lookup Zon erstellung bis nach dem Schritt Primary Zone auswählen. Beim nächsten Schritt wähle IPv4 Reverse-Lookup Zone aus:


![Lokales Bild](/images/IPv4%20reverse%20lookup%20zone.jpg)




2.1 Network ID definieren:

![Lokales Bild](/images/Network%20ID%20definieren.jpg)


2.2 Beim nächsten Schritt lasse alles wie es ist und gehe weiter. Wähle wieder do not allow dynamics updates aus und zum schluss beende die erstellung der reverse-Lookup Zone.



### Erstellung Datensatz
Hier werde ich euch zeigen, wie man einen A- und MX-Datensatz erstellt.

1. A-Datensatz erstellen für Server:
Rechtsklick auf den Forward-Lookup Bereich und dann new Host anwählen. Stelle dies dann wie folgt ein:

![Lokales Bild](/images/New%20Host%20server.jpg)



1.1 Zweiter A-Datensatz erstellen für Client:
Rechtsklicke auf den Forward-Lookup Bereich und wähle dann new Host an. Stelle dies dann wie folgt ein:


![Lokales Bild](/images/new%20host%20client.jpg)






2. MX-Datensatz erstellen:
Wieder Rechtsklick auf die Forward-Lookup Bereich und dann New Mail Exchange auswählen. Dann wie folgt einstellen:

![Lokales Bild](/images/Mail%20exchange.jpg)




### DNS Anfrage weiterleitung von Windows Server zu Google DNS:
Wir stellen jetzt ein das wen du ein DNS anfrage stellst mit eine Externen Adresse das dein lokaler DNS die anfrage an den Google DNS weiterleit.
Rechtsklicke auf DNS im DNS Manager und wähle dann Proporties:

![Lokales Bild](/images/rechtsklicke%20dns%20Serve.jpg)




Gehe dann auf Forwarders und füge dort den Google DNS hinzu. Dies sollte dann so aussehen:

![Lokales Bild](/images/Google%20Dns%20hinzufügen.jpg)






### Überprüfung
Öffne Powershell als Administrator. 
Gib diese Befehle ein:
- nslookup client.itzh.ch
- nslookup server.itzh.ch
- nslookup mail.itzh.ch 
- nslookup youtube.ch


Wen es dann so ausieht hast du alles richtig konfiguriert:¨

![Lokales Bild](/images/Überprüfung%20dns.jpg)





## Client Konfiguration:
Du kannst hier den aufgesetzten Client nehmen den wir im Module 117 aufgesezt haben.
Stelle den Adapter Costum (M117_Lab) so ein:

![Lokales Bild](/images/costum%20m117lab%20adapter.jpg.png)



Stelle den Nat adapter so ein:

![Lokales Bild](/images/nat%20adapter%20einstellung.jpg)











## Überprüfung
Öfffne Powershell als Administrator. 
Benutz folgende Forward Befehle:
- nslookup server.itzh.ch
- nslookup client.itzh.ch
- nslookup mail.itzh.ch
- nslookup youtube.com


Wen die ausgabe so aussieht hast du alles richtig gemacht:


![Lokales Bild](/images/forward%20anfrage%20client.jpg)




Benutze folgende Reverse Befehle:
- nslookup 192.168.100.170
- nslookup 192.168.100.153


Wen die ausgabe so aussieht hast du alles richtig gemacht:

![Lokales Bild](/images/reverse%20anfrage%20client.png)












