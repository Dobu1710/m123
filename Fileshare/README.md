# Fileshare Konfiguration Ubuntu Desktop
## Vorbereitung
Punkt 1 :

Als erstes brauchst du ein VM mit aufgesetzten Ubuntu Desktop. Dieses Vm ist der Server. Du musst der Netzwerkkarte Costum_M117 ein statische IP zuweisen.

Punkt 2:

Du brauchst ebenfalls eine VM mit aufgeseztem Windows 10. Diese VM gilt als Windows Client. Du musst dem Netzwerkapter Costum_M117 
ebenfalls ein statische IP zuweissen.

Schrit 3:

Pinge mit dem Client den Server. Wen der Ping erfolgreich war kannst du mit der Konfiguration des Samba Share beginnen.

## Steps to install SAMBA on Ubuntu 22.04 LTS
1. Systemaktualisierung ausführen
 ```bash
sudo apt update
 ```

2. Samba installieren:
 ```bash
sudo apt install samba
 ```

Überprüfung ob der Dienst aktiv ist und ausgeführt wird:
 ```bash
systemctl status smbd --no-pager -l
 ```

Wen diese so aussieht wird der Dienst ausgeführt und ist aktiv:


![Beispielbild](/images/Screenshot%202024-01-21%20140351.pg.png)


Nutzen sie Folgenden Befehl um den Dienst so zu aktivieren das er bei jedem Systemstart automatisch gestartet wird:
 ```bash
sudo systemctl enable --no smbd
 ```


 3.  Samba in der Firewall von Ubuntu zulassen:
  ```bash
sudow ufw allow samba
 ```

4. Benuztzer zu Samba Gruppe hinzufügen
Hier fügen wir den aktuellen Systembenutzer der Samba Grupe hinzu. Somit können auf alle Dateien und Ordner zugegriffen werden, die unter ihm freigeben sind.

 ```bash
sudo usermod -aG sambashare $USER
 ```

Nach dem sie den Command eingeben haben könnnen Sie ein Kennwort festlegen. Geben Sie das Passwort ein bestätigen Sie es. Das Kennwort muss nicht identisch zu ihrem Systembenutzerkennwort sein.

 ```bash
sudo smbpasswd -a $USER
 ```

 5. Ubuntu Ordner freigeben
 Machen sie ein Rechtsklick auf den Ordner Pictures und wählen Sie Proporties:

 ![Beispielbild](/images/Folder%20sharing%201.jpg)



 Nach dem sich das Fenster geöffnet hat klicke auf die Registerkarte lokale Netzwerkfreigabe.
 Stelle Sie wie folgt ein und klicke danach auf Creat Share:

 ![Beispielbild](/images/Folder%20sharing%202.jpg)



 Zum Schluss bestätig mit Berechtigung automatische hinzufügen:

 ![Beispielbild](/images/Folder%20Sharing%203.jpg)


 ## Windows Client
 Wir werden jetzt den Freigeben Ubuntu Ordner auf unserem Windows Client mit Hilfe von Samba mounten.
 Öffnen sie ihren Explorer und klicken Sie auf dieser PC. Danach gehen Sie auf die Registerkarte Computer und wählen Netzwerkadresse hinzufügen. Dies sieht dann so aus:

 ![Beispielbild](/images/ad%20netzwerklocation.jpg)


Es öfnnet sich dann ein Fenster klicke auf Next und auf dem kommend ebenfalls. Beim 3 Fenster gibt die Ip von deinem Server und der Ordner wo freigegeben hast an. Dies sollte dann so aussehen:

![Beispielbild](/images/IP%20und%20Ordner%20angeben.jpg)


Danach wirst du aufgefordert dein Samba Benutzernamen und Kennwort eingeben und danach klicke auf weiter:

![Beispielbild](/images/benutzer%20und%20passwort%20abfrage.jpg)


Beim folgenden Fenster bestätige mit Next:

![Beispielbild](/images/Namen%20network%20location.jpg)



Das folgende Fenster ist das letzte. Klicke auf Finish und schliesse so das hinzufügen des Netzwerkstandortes ab:

![Alt text](/images/Fileshaar%20Windows%20Client%20finish.jpg)

## Überprüfung
Erstelle im geteilten Ordner auf dem Windows Client ein File. Danach bearbeitet es.
Überprüfe ob diese änderung auf dem Server angezeigt werden wen ja funktioniert alles so wie es sollte.