# Splendid Blur AG Auftrag Filius
## Vorbereitung:
Verbinde und beschriftet all Geräte gemäss Vorgabe. Dies sollte dann so aussehen:

![Lokales Bild](/images/Gerät%20beschriften%20und%20verbinden.jpg)





## Konfiguration DHCP&Client GL,Buchhaltung und Einkauf
Wähle bei allen Client das Kästchen DHCP Konfiguration verwenden an.
Beim PC21 müssen gemäss Vorgabe ein Statische IP vergeben. Stelle den Client wie folgt ein:

![Lokales Bild](/images/Statische%20Ip.jpg)


### DHCP einrichten:
Wähle den DHCP Server an und klicke das Feld DHCP einrichten an. Dann stelle den DHCP wie folgt ein:

![Lokales Bild](/images/Dns%20einrichten%20.jpg)



Bei Statische Adresszuweisung musst du nun den Client mit Statischer IP hinzufügen:

![Lokales Bild](/images/statische%20adresszuweisung.jpg)




## Konfiguration Client Logistick
Richt den Client PC51 so ein :

![Lokales Bild](/images/Client%20PC51.jpg)




Richte den Client PC52 wie folgt ein:

![Lokales Bild](/images/Client%20PC52.jpg)



## Konfiguration Web Server und DNS:
Stelle den DNS wie folgt ein:

![Alt text](/images/DNS%20Konfiguration.jpg)





Richte den Webserver so ein:

![Alt text](/images/Web%20Server%20Konfiguration.jpg)





## Zwischenzeitlicher Ping Test:
Jetzt machen von einem DHCP Client und schauen ob der DHCP Prozess und die Geräte sich alle Pingen können. Wen dies bei euch so aussieht sollte alles richtig gemacht sein:

![Lokales Bild](/images/Zwischenzeitlicher%20Ping%20test.jpg)



## Web Server und DNS einrichten
Als erste installiere auf dem Web Server das Feature Web Server. Dann öffne das Feature und klicke auf Starten:

![Alt text](/images/Web%20Server%20starten.jpg)





Nachdem wir den Webserver eingerichtet haben müssen wir den DNS einrichten. Als erste wähle den DNS und installiere mit der Software Installation DNS Server.
Danach suche dir ein Web adresse ich habe mich hier für splendidblur-ag.ch mit der IP 172.16.100.4 entschieden. Dies sieht dann so aus:

![Alt text](/images/DNS%20einrichten%202.jpg)






## DNS und Web Server Testen:
Geh auf einen Client und klicke dort auf Webbrowser und gebe dort die Adresse splendidblur-ag.ch. Wen es dann so aussieht hast du alles richtig gemacht:

![Alt text](/images/DNS%20und%20Webserver%20Test%20.jpg)





## Mögliche Änderungen:
Also man könnte nur ein Subnetz mache und alle Clients mit DHCP .



















