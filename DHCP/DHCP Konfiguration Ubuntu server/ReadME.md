# Ubuntu Server installieren und DHCP konfigurieren
## Netzwerkadapter hinzufügen
1. Füge einen Netzwerkadpter hinzu und stelle ihn auf das M123_Lab ein

![Lokales Bild](/images/Netzwerkadapter.jpg)

1.1 Klicken sie Edit an und wählen Virtual Netzwork Editor an. Danach klicken sie den Netzwerk Adapter VMnet8 an und stellen ihn folgen ein:

![Alternativer Text](/images/VMnet8%20Adapter%20einstellungen.jpg)


Wen dies gemacht ist stellen sie den anderen Adapter M117_Lab folgend ein:

![Alternativer Text](/images/M117LAB%20adapter%20einstellungen.jpg)

Wen Sie dies gemacht haben kann man mit der Installation vom Betriebsystem Ubuntu Server beginnen.


# Installation Ubuntu Server
1. Dies ist unser Startbildschirm, klicken Sie "try install ubuntu server" an.


![Alternativer Text](/images/5.jpg)

2. Die weiteren Schritt sind relativ simple nachzufolziehen. Sie können sich eigentlich durchklicken.

3. Die Storage Configuration solle wie folgt aussehen:


![Alternativer Text](/images/6%20(1).jpg)


4. Das eingeben des Namens und die Benutzereinstellung ist ihnen überlassen:

![Alternativer Text](/images/7.jpg)

5. Nach ein paar weiteren Schritten wird dann das Betriebsystem Ubuntu Server installiert. Dies könnte eine Weile dauern. Nach der Installation plop ein Terminel auf bei dem Sie den vorhin definierten Benutzernamen und das Passwort eingeben müssen. Jetzt sind fertig mit der Installation des Betriebsystem und sie können mit der Konfiguration vom DHCP beginnen.


# Konfiguration DHCP
## Installation DHCP Server

1. Als erstes sollt man den DHCP Package herunterladen und installieren. Bevor man dies aber tut sollt man  besten die Package List zuerst aktualisieren. Dies macht mann wie folgt:

 ```bash
sudo apt update
sudo apt upgrade
 ```

2. DHCP Server Package installieren

 ```bash
sudo apt install isc-dhcp-server
 ```
## DHCP Server konfigurieren
1. Um den DHCP Server zu konfigurieren braucht muss man verschiedene Dateien bearbeiten:
1.1 dhcp.conf:
Diese Datei ist für die Konfiguration der IP Range und deren Leasetime verantwortlich. Sie befindet sich unter /ect/dhcp/dhcpd.conf
Vor dem Bearbeiten erstell man immer zuerst eine Kopie vom Orginal und dies mancht mach wie folgt.

 ```bash
sudo cp dhcpd.conf dhcpd.conf. cp
 ```
1.2 Datei öffnen und bearbeiten
- Als erstes sollte man in dieser Datei die Lease Time in Sekunden bearbeiten. Dies habe ich auf 8 Tage also 691200 Sekunden Gesetz. Beim haupt DHCP Server im Netzwerk muss beim Kommentar # authorative das # entfernt werden. So sollt dies nachher aussehen:

![Alternativer Text](/images/Dhcp%20config%20file%201.jpg)

- Sie müssen ebenso in der Datei die einfachste Konfiguration benutzen. Es sollte reichen wen sie das nur Subnet und die Ip Range angeben. In diesen Fall ist das Subnet 192.168.100.0, die Netzmaske ist 255.255.255.0 und der IP Range ist 192.168.100.151-192.168.100.200.
Das Bild zeigt ihnen wie es nachher ausehen soll:

![Alternativer Text](/images/dhcp%20sconfig%20file%202.jpg)

- Sie alle Anpassungen richtig gemacht haben Speichern sie die Datei.

2. Namen Netzwerk Interfaces festelegen
- Datei öffnen:
 ```bash
sudo nano /etc/default/isc-dhcp-server
 ```
 Beim INTERFACESv4 soll anstatt eth'0 -> Ens37 stehten. Wir geben hier vor das Ens37 der Netzwerkadpter für den DHCP- Request sein soll. Dann Speichern Sie die Datei.

 3. Zweiten Netzwerkkarte(Internes Netzwerk) fixe Ip Zuweisen
 Datei öffnen:
  ```bash
sudo nano /etc/netplan/00-installer-config.yaml
 ```

Bei Ens37 bestimmen wir das unser Server eine Statische IP-adresse bekommt.
Beim Ens33 geben wir nur an dass die IP Adresse über DHCP bezogen wird.
Die sollte dann folgender massen ausehen:

![Alternativer Text](/images/Interface.jpg)

4. Zu dem letzen Schritten gehört das Rebooten unseres DHCP servers. 
Dies geschieht mit :
  ```bash
sudo systemctl restart isc-dhcp-server
 ```
Den Status des DHCP-Server prüfen sie folgendermassen:
  ```bash
sudo systemctl status isc-dhcp-server
 ```
Nach diesem Schritt sollte sie eigentlich die Statusmeldung Active running erhalten

Bei mir kam aber leider nicht die Meldung Active running. Sondern es kam die Meldung Active Failed

Da es bei mir so aussieht führen wir hier die Anleitung nicht weiter for sonderen beginnen mit dem Troubelshooting.

## Troubelshooting
### Schritt 1
Punkt 1: Ich habe alle Datein nochmal angeschaut und überprüft ob ich alles richtig eingegeben.habe.

Punkt 2: Ich habe die Netzwerkadapter im Interfacev4 und im und /ect/netplan/00-installer-config.
yaml gewechselt und nochmals geschaut bracht immernoch nichts.

Punkt 3: Ich habe ihm /etc/dhcp/dhcpd.config der Release Time angepasst.

Punkt 4: Ich habe die PID Fehlermeldung genauer angeschaut konnte aber daraus wenig schlüsse ziehen.

Nach diesem Schritt dachte ich das es die Falsche anleitung ist und habe eine neue VM erstellt mit der anleitung vom ehemaligen Schüler benutzt.

### Schritt 2
Mein erster Punkt war wieder die Dateien anschauen und überprüfen und ich fand wieder.

Mein zweiter Punkt war erneut die Netwerkadpter in diesen beiden Files zu tauschen.

Meine dritter Punkt war wieder die Realese Time wieder zu erhöhen brachte erneutes nicht.

### Schritt 3
Nach diesem Schritt habe ich ein Anleitung gefunden die am Anfang schon dem Netzwerkadpter eine fix IP zweisst. Dann habe ich dies gemacht es funktioniert immernoch nicht.
Der Status änderte sich nicht und ich hatte langsam keine Ideen mehr und habe es dann nicht mehr weiter geschaft.

