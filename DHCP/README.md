# Dokumentation DHCP Server in Filius Konfigurieren

## Geräte erstellen
- erstelle 4 Notebooks
- 1 Switch
- 1 Rechner



## Geräte verbinden
- Verbinde alle Clients mit der Switch
- **Verbinde die Swicht mit dem Rechner**



## Geräte umbennung
| Gerät | Name |
|----------|----------         |
| Laptop   | Client 1 ( DHCP)  |
| Laptop    | Client 2 ( DHCP) |
| Laptop   | Client 2 ( DHCP)  |
| Laptop  | Client 4 ( Statische IP 192.168.0.50)                            |
| Switch  | Switch / WLAN      |
| Rechner | DHCP Server        |


## Konfiguration Geräte


1. Konfiguration DHCP Server:

  ```bash
 - DHCP Server anwählen
 - DHCP Server einrichten anklicken
 - DHCP aktivieren 
1.1 Grundeinstellung: 
> Adresse-Untergrenze = 192.168.0.150
> Adress-Obergrenze =  192.168.0.170
 Stattische Adresszuweisung:
> IP 192.168.0.50 eingeben
> MAC adresse von Client 4 Kopieren und einfügen
 Hinzufügen anklicken.
DHCP Aktivieren:
>Bei grundeinstellung DHCP aktivieren anklicken und mit OK bestäigen
 ```


2. Konfiguration Clients:

 ```bash
- Wähle alle Clients an und wähle das Kästen DHCP zur Konfiguration verwenden
 ```



## Anleitungsvideo Youtube:
**Link:**
[FiliusTV: DHCP einrichten und verwenden](https://www.youtube.com/watch?v=G-TXiRSqEzg)



















